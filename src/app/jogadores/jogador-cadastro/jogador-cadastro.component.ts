import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';

import { ToastyService } from 'ng2-toasty';
import { ErrorHandlerService } from './../../core/error-handler.service';
import { JogadoresService } from '../jogadores.service';
import { Jogador } from 'app/core/model';

@Component({
  selector: 'app-jogador-cadastro',
  templateUrl: './jogador-cadastro.component.html',
  styleUrls: ['./jogador-cadastro.component.css']
})
export class JogadorCadastroComponent implements OnInit {

  cadastro = new Jogador();

  constructor(
    private jogadorService: JogadoresService,
    private toasty: ToastyService,
    private errorHandler: ErrorHandlerService,
    private title: Title,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.title.setTitle('Cadastra novo Jogador');
  }

  salvar(form: FormControl) {
    this.adicionarJogador(form);
  }

  adicionarJogador(form: FormControl) {

    this.jogadorService.salvarJogador(this.cadastro)
      .then(() => {
        this.toasty.success('Jogador salvo com sucesso!');
        form.reset();
        this.cadastro = new Jogador();
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  novo(form: FormControl) {
    form.reset();
    setTimeout(function () {
      this.cadastro = new Jogador();
    }.bind(this), 1);
    this.router.navigate(['/jogador/novo]']);
  }
}

