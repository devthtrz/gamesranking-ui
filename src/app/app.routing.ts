import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { PaginaNaoEncontradaComponent } from './core/pagina-nao-encontrada.component';
import { JogadoresPesquisaComponent } from './jogadores/jogadores-pesquisa/jogadores-pesquisa.component'
import { JogadorCadastroComponent } from './jogadores/jogador-cadastro/jogador-cadastro.component';
const APP_ROUTES: Routes = [

  { path: '', redirectTo: 'jogadores', pathMatch: 'full' },
  { path: 'jogadores', component: JogadoresPesquisaComponent },
  { path: 'jogador/novo', component: JogadorCadastroComponent },
  { path: 'pagina-nao-encontrada', component: PaginaNaoEncontradaComponent },
  { path: '**', redirectTo: 'pagina-nao-encontrada' }

];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
