import { GamesrankingUiPage } from './app.po';

describe('gamesranking-ui App', () => {
  let page: GamesrankingUiPage;

  beforeEach(() => {
    page = new GamesrankingUiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
