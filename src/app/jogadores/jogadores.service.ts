import { Injectable, SystemJsNgModuleLoader } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http'
import { environment } from './../../environments/environment';

import 'rxjs/add/operator/toPromise';

import { Jogador } from './../core/model';

export class JogadorFiltro {
  jogador: string;
  vitorias: number;
  partidas: number;

}

@Injectable()
export class JogadoresService {

  jogadorUrl: string;

  constructor(private http: Http) {

    this.jogadorUrl = `${ environment.apiUrl }/ranking`;
  }

  salvarJogador(jogador: Jogador): Promise<Jogador> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.jogadorUrl, JSON.stringify(jogador), { headers })
      .toPromise()
      .then(response => response.json());
  }

  listaJogadoresOrdemVitorias(): Promise<any> {

    return this.http.get(`${ this.jogadorUrl }/lista`)
      .toPromise()
      .then(response => response.json());

  }

  adicionaVitoria(jogador: Jogador): Promise<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.put(`${ this.jogadorUrl }/adicionarvitoria`, JSON.stringify(jogador), { headers })
      .toPromise()
      .then(() => null);
  }

  adicionaPartida(jogador: Jogador): Promise<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.put(`${ this.jogadorUrl }/adicionarpartida`, JSON.stringify(jogador), { headers })
      .toPromise()
      .then(() => null);
  }


}
